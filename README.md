Ionic2-Habit
======
## Introduction

Simple and posite app just for fun

## Software stack:

#### To run this project we are using next technologies:

* [Ionic ^2.2.1](https://ionicframework.com/docs/v2/cli/)
* [Angular ^2.2.1](https://angular.io/)
* [TypeScript ^2.0.9](https://www.typescriptlang.org/)

#### API - used in app
* [Quotes](http://quotes.stormconsultancy.co.uk)
* [Json-Server](https://github.com/typicode/json-server)

## Project structure:

````
├── resources                          * folder with supported platforms
│
├── src                                * main source folder
│	├── app                                 * folder with main where all connect to one
│	├── assets                              * folder with all nesessary img and etc... files
│	├── pages                               * folder with pages(components) of our app
│	│	├── expand-stack						* folder with list of habits and one category with actions
│	│	├── home                       			* folder with file's for start page intro and connections
│	│	├── inspire								* folder with list of inspire quotes from API
│	│	├── settings				        	* folder with main app componet, polyfills, verdor
│	│	├── tabs                       			* folder with setting for footer tabs
│	│	└── todo-habit							* folder with files for choose list of todo habits
│	│
│	├── shared                         		* folder with where will store different services for our app
│	│	├── api									* folder with api constant url
│	│	└── services 							* folder with needed services 
│	│
│	├── theme                               * folder with all import scss styles for beatufy app
│	├── declaration.d.ts                    * file where Typescript know all about your code
│	├── index.html                          * index.html
│	├── manifest.json                       * file with settings for our icon
│	└── service-worker.js                   * sw-toolbox to custom configure your service worker
│
├── README.md                               * markdown about project
├── database.json                           * file for json-server to mock API for habits
├── config.xml                              * !important for cordova - cross platform configuration
├── ionic.config.json                       * !important ionic config enable some features
├── package.json
├── tsconfig.json
└── tslint.json

````
## Quick start:
    
```bash
# clone our repo

git clone https://github.com/vitalikryzhenkous/ionic2-habits.git || download zip .. etc

# change directory to our repo
cd ionic2-habit

# install the repo with npm or with yarn
npm install

# start the server
npm run json-api

# start the server
ionic serve

```