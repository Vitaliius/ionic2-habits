import { Component } from '@angular/core';
import { Toggle } from 'ionic-angular';
import { SettingsService } from '../../shared/services/settings.service'


@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

	constructor(private settingsService: SettingsService) {}
	isToggleChange(toggle: Toggle) {
		this.settingsService.setToggleChange(toggle.checked);
	}

	onToggleChange() {
		return this.settingsService.getToogleChange();
	}
}
