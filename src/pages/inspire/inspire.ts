import { Component } from '@angular/core';
import {QuotesService} from "../../shared/services/quotes.service";
import {IqoutesRandom, IqoutesPopular} from "./IqoutesRandom";
@Component({
  selector: 'page-inspire',
  templateUrl: 'inspire.html'
})
export class InspirePage {
  public quoPopArr: IqoutesPopular;
  public quoRan: IqoutesRandom;

  constructor(private quotesService: QuotesService) {}

  ngOnInit() {
    this.getPop();
    this.getRan();
  }

  /*
  * we invoke throw service and set response to  quoPopArr
  */
  getPop() {
    this.quotesService.getPopularQuotes()
      .subscribe(
        res => { this.quoPopArr = res; console.log('Pop ', res, res.length / 4) },
        err => console.log(err)
      )
  }

  /*
   * we invoke throw service and set response to  quoRan
   */
  getRan() {
    this.quotesService.getRandomQuotes()
      .subscribe(
        res => { this.quoRan = res; console.log('Ran ', res);  },
        err => console.log(err)
      )
  }

}
