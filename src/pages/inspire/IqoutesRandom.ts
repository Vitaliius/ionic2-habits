export interface IqoutesRandom {
  id: number,
  author: string,
  quote: string,
  permalink: string
}

export interface IqoutesPopular {
  [index: number]: IqoutesRandom;
}

