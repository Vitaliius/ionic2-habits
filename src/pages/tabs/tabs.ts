import { Component } from '@angular/core';
import {HomePage} from "../home/home";
import {ExpandStackPage} from "../expand-stack/expand-stack";
import {InspirePage} from "../inspire/inspire";
import {SettingsPage} from "../settings/settings";
import {TodoHabitPage} from "../todo-habit/todo-habit";
@Component({
  selector: 'page-tabs',
  template: `
    <ion-tabs selectedIndex="0">
        <ion-tab [root]="homePage" tabTitle="Home" tabIcon="ios-planet-outline"></ion-tab>
        <ion-tab [root]="todoHabitPage" tabTitle="TODO" tabIcon="logo-tux" tabBadge="5"></ion-tab>
        <ion-tab [root]="inspirePage" tabTitle="Inspiration" tabIcon="ios-flame-outline"></ion-tab>
        <ion-tab [root]="expandStackPage" tabTitle="Habits-List" tabIcon="ios-list-box-outline"></ion-tab>
        <ion-tab [root]="settingsPage" tabTitle="Settings" tabIcon="ios-settings-outline"></ion-tab>
    </ion-tabs>
    
  `
})
export class TabsPage {
  homePage = HomePage;
  todoHabitPage = TodoHabitPage;
  expandStackPage = ExpandStackPage;
  inspirePage = InspirePage;
  settingsPage = SettingsPage;
}
