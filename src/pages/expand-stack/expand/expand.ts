import { Component } from '@angular/core';
import { AlertController , NavController, NavParams } from 'ionic-angular';
import {Ihabits} from "./Ihabits";
import {ActionsHabitsService} from "../../../shared/services/actions.habits.service";

@Component({
  selector: 'page-expand',
  templateUrl: 'expand.html'
})
export class ExpandPage {
  public habitObj: Ihabits;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private todoService: ActionsHabitsService
  )  {}

  ngOnInit() {
    this.habitObj = this.navParams.get('singleHabit');
  }


  addToDoHabit(val: Ihabits) {
    const alert = this.alertCtrl.create({
      title: 'Add Habit',
      subTitle: 'Are you think - you can do this?',
      buttons: [
        {
          text: 'Yes, please add this habit',
          handler: () => {
            console.log('ok');
            this.todoService.addTodo(val);
          }
        },
        {
          text: 'No, please',
          role: 'cancel',
          handler: () => {console.log('no')}
        }
      ]
    });
    alert.present();
  }


  ifAddToDo(val: Ihabits) {
    return this.todoService.isToDoAdd(val);
  }

  wasRemoveToDo(val: Ihabits) {
    this.todoService.removeTodo(val);
  }

}
