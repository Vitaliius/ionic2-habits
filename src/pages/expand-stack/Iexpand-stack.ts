import {Ihabits} from "./expand/Ihabits";

export interface IexpandStack {
  [index: number]: {
    categories: string,
    habits: Ihabits,
    icon: string
  }
}

