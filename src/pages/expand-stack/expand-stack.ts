import { Component } from '@angular/core';
import {HabitsService} from "../../shared/services/habits.service";
import {IexpandStack} from "./Iexpand-stack";
import {NavController} from "ionic-angular";
import {Ihabits} from "./expand/Ihabits";
import {ExpandPage} from "./expand/expand";
@Component({
  selector: 'page-expand-stack',
  templateUrl: 'expand-stack.html'
})
export class ExpandStackPage {
  public getHabitsData: IexpandStack[];

  constructor(private habService: HabitsService, private navCtrl: NavController) {}

  ngOnInit() {
    this.getMockHabits();

  }

  getMockHabits() {
    this.habService.getHabits()
      .subscribe(
        data => {this.getHabitsData = data},
        err => console.log(err)
      )
  }

  goToExpandPage(val:Ihabits) {
    this.navCtrl.push(ExpandPage, {singleHabit: val});
  }
}
