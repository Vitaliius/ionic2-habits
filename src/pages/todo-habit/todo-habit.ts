import { Component } from '@angular/core';
import { Ihabits } from "../expand-stack/expand/Ihabits";
import { ActionsHabitsService } from "../../shared/services/actions.habits.service";
import { ViewController, AlertController} from "ionic-angular";
import { SettingsService } from "../../shared/services/settings.service";

@Component({
  selector: 'page-todo-habit',
  templateUrl: 'todo-habit.html'
})

export class TodoHabitPage {
  public todosArr: Ihabits[];
  constructor(private todoService: ActionsHabitsService,
              private settingsService: SettingsService,
              private alertCtrl: AlertController,
              private viewCtrl: ViewController) {}

  ionViewWillEnter() {
    this.todosArr = this.todoService.getTodo();
  }

  removeToDo(val: Ihabits) {
    const alert = this.alertCtrl.create({
      title: 'Remove Habit',
      subTitle: 'Are you sure?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            console.log('remove happen');
            this.todoService.removeTodo(val);
            this.todosArr = this.todoService.getTodo();
          }
        },
        {
          text: 'No',
          role: 'cancel',
          handler: () => {console.log('no remove')}
        }
      ]
    });
    alert.present();
  }


  getAltBg() {
    return this.settingsService.getToogleChange() ? 'altHabitBG' : 'habitBG';
  }


}
