export const API_CONST = {
  // Quotes url
  quotes_popular_url: 'http://quotes.stormconsultancy.co.uk/popular.json',
  quotes_random_url: 'http://quotes.stormconsultancy.co.uk/random.json',

  // Habits url
  habits_url: 'http://localhost:3000/expands'
};
