import {Injectable} from '@angular/core';
import { Http } from "@angular/http";
import 'rxjs/Rx';

import { API_CONST } from '../api/api.const';

@Injectable()

export class QuotesService {
  private url_pop: any = API_CONST.quotes_popular_url;
  private url_random: any = API_CONST.quotes_random_url;
  constructor(private http: Http) {}

  /*
  * Get from api response with popular quotes
  */
  getPopularQuotes() {
   return this.http.get(this.url_pop)
      .map(res => res.json())
  }

  /*
  * Get from api response with random quotes
  */
  getRandomQuotes() {
    return this.http.get(this.url_random)
      .map(res => res.json())
  }

}
