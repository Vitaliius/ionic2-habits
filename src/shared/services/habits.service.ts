import {Http} from "@angular/http";
import {Injectable} from "@angular/core";
import 'rxjs/Rx';
import {API_CONST} from "../api/api.const";

@Injectable()

export class HabitsService {
  private hab_url = API_CONST.habits_url;

  constructor(private http: Http){}

  getHabits() {
    return this.http.get(this.hab_url)
      .map(res => res.json())
  }


}
