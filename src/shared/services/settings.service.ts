import {Injectable} from "@angular/core";
import 'rxjs/Rx';

@Injectable()

export class SettingsService {
  public changeBackground: boolean = false;

  setToggleChange(isToggle: boolean) {
  	this.changeBackground = isToggle;
  }

  getToogleChange() {
  	return this.changeBackground;
  }

}
