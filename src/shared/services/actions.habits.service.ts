import {Injectable} from "@angular/core";
import {Ihabits} from "../../pages/expand-stack/expand/Ihabits";

@Injectable()

export class ActionsHabitsService {
  private  todos: Ihabits[] = [];

  addTodo(val: Ihabits) {
    this.todos.push(val);
    console.log(this.todos);
  }

  removeTodo(val: Ihabits) {
    const position = this.todos.findIndex((valEl) => {
      return valEl.id === val.id;
    });
    this.todos.splice(position, 1)
  }

  getTodo() {
    return this.todos.slice();
  }

  isToDoAdd(val: Ihabits) {
    return this.todos.find((valEl: Ihabits) => {
      return valEl.id === val.id;
    });
  }
}
