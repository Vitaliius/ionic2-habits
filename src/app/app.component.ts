import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { TabsPage } from "../pages/tabs/tabs";
import { QuotesService } from "../shared/services/quotes.service";
import { HabitsService } from "../shared/services/habits.service";
import { ActionsHabitsService } from "../shared/services/actions.habits.service";
import { SettingsService } from "../shared/services/settings.service";



@Component({
  templateUrl: 'app.html',
  providers: [QuotesService, HabitsService, ActionsHabitsService, SettingsService]
})
export class MyApp {
  rootPage = TabsPage;

  constructor(platform: Platform) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }
}
