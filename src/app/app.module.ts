import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { TabsPage } from '../pages/tabs/tabs';
import { HomePage } from "../pages/home/home";
import { ExpandPage } from "../pages/expand-stack/expand/expand";
import { ExpandStackPage } from "../pages/expand-stack/expand-stack";
import { InspirePage } from "../pages/inspire/inspire";
import { SettingsPage } from "../pages/settings/settings";
import {TodoHabitPage} from "../pages/todo-habit/todo-habit";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TodoHabitPage,
    ExpandPage,
    ExpandStackPage,
    InspirePage,
    SettingsPage,
    TabsPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TodoHabitPage,
    ExpandPage,
    ExpandStackPage,
    InspirePage,
    SettingsPage,
    TabsPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
